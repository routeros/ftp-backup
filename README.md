# Backup script for RouteOS

Change variables in the beginning of the script to your own server

### Quick installation:

```
/system script
add name=ftp-backup source="{\
    \n\t:local backupFTPhost 0.0.0.0;\
    \n\t:local backupFTPport 21;\
    \n\t:local backupFTPuser user;\
    \n\t:local backupFTPpassword password;\
    \n\t/log info \"[Backup] Starting\";\
    \n\t:local date [/system clock get date]\
    \n\t:local backupfile ([/system routerboard get serial-number].\"-\".[:pick \$date 7 11].[:pick \$date 0 3].[:pick \$date 4 6].\".rsc\")\
    \n\t/export file=\$backupfile;\
    \n\t/log info \"[Backup] Waiting few minutes while backup is generated\";\
    \n\t:delay 300s\
    \n\t/log info \"[Backup] Starting upload to FTP server\";\
    \n\t/tool fetch address=\$backupFTPhost port=\$backupFTPport user=\$backupFTPuser password=\$backupFTPpassword mode=ftp src-path=\$backupfile dst-path=\"/upload/\$backupfile\" upload=yes;\
    \n\t/log info \"[Backup] Upload finished\";\
    \n\t:delay 10s\
    \n\t/log info \"[Backup] Deleting backup file\";\
    \n\t/file remove \$backupfile;\
    \n\t/log info \"[Backup] Done\";\
    \n}"
/system scheduler
add interval=1d name=backup on-event=ftp-backup start-date=jan/01/1970 start-time=04:00:00
```
