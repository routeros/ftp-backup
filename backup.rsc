{
	:local backupFTPhost 0.0.0.0;
	:local backupFTPport 21;
	:local backupFTPuser user;
	:local backupFTPpassword password;
	/log info "[Backup] Starting";
	:local date [/system clock get date]
	:local backupfile ([/system routerboard get serial-number]."-".[:pick $date 7 11].[:pick $date 0 3].[:pick $date 4 6].".rsc")
	/export file=$backupfile;
	/log info "[Backup] Waiting few minutes while backup is generated";
	:delay 300s
	/log info "[Backup] Starting upload to FTP server";
	/tool fetch address=$backupFTPhost port=$backupFTPport user=$backupFTPuser password=$backupFTPpassword mode=ftp src-path=$backupfile dst-path="/upload/$backupfile" upload=yes;
	:delay 30s
	/log info "[Backup] Deleting backup file";
	/file remove $backupfile;
	/log info "[Backup] Done";
}
